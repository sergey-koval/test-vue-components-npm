module.exports = {
  pages: {
    index: {
      entry: 'dev-server/main.js',
      template: 'dev-server/index.html',
      filename: 'index.html',
      title: 'Index Page',
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  }
}